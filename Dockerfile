FROM myriadmobile/alpine-laravel:5.x-php7.1
MAINTAINER Myriad Web Team <web@myriadmobile.com>

ADD . /var/www/

RUN composer install --ansi --no-dev --no-interaction --no-progress --no-suggest