<?php
namespace App\Console\Commands;

use App\Services\Import;
use Illuminate\Console\Command;

class ImportPokedex extends Command {
  /**
   * The name and signature of the console command.
   * @var string
   */
  protected $signature = 'import:pokedex';

  /**
   * The console command description.
   * @var string
   */
  protected $description = 'Import Pokedex data';

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle() {
    (new Import)->executePokedex();
    print("Done Importing Pokedex" . PHP_EOL);
  }
}
