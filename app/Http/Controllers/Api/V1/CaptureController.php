<?php
namespace App\Http\Controllers\Api\V1;

use App\Exceptions\ApiBadRequestException;
use App\Exceptions\ApiModelNotFoundException;
use App\Http\Resources\PokemonCollection;
use App\Models\Pokemon;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \Exception;
use App\Models\Capture;
use App\Repositories\CaptureRepository;
use App\Repositories\PokemonRepository;

class CaptureController extends Controller {
  /**
  * index - get captures by the user
  * @return PokemonCollection
  * @throws ApiModelNotFoundException
  */
  public function index(CaptureRepository $captureRepository) {
    return new PokemonCollection(collect($captureRepository::byUser(auth()->user()->id)->pluck('pokemon')));
  }

  /**
   * store - create a pokemon capture
   * @param Request $request
   * @return JsonResponse
   * @throws ApiBadRequestException
   */
  public function store(CaptureRepository $captureRepository, Carbon $carbon, Request $request) {
    $capture = $captureRepository::create($request);
    return new JsonResponse(['message' => "Pokemon has been captured! {$capture->pokemon->name} - {$carbon::now()}"]);
  }
}
