<?php
namespace App\Http\Controllers\Api\V1;

use App\Exceptions\ApiModelNotFoundException;
use App\Http\Resources\PokemonCollection;
use App\Http\Resources\PokemonResource;
use App\Http\Controllers\Controller;
use App\Models\Pokemon;
use \Exception;
use App\Repositories\PokemonRepository;

class PokedexController extends Controller {

  /**
   * index - get pokemon list
   * @return PokemonCollection
   * @throws ApiModelNotFoundException
   */
  public function index() {
    return new PokemonCollection(Pokemon::paginate(config('app.per_page')));
  }

  /**
   * show - get pokemon by identifier
   * @return PokemonResource
   * @throws ApiModelNotFoundException
   */
  public function show(PokemonRepository $pokemonRepository, $pokemonId) {
    return new PokemonResource($pokemonRepository::byIdentifier($pokemonId));
  }

  /**
   * byName - get pokemon by name
   * @param string $name
   * @return PokemonCollection
   * @throws ApiModelNotFoundException
   */
  public function byName(PokemonRepository $pokemonRepository, $name) {
    return new PokemonCollection($pokemonRepository::byName($name));
  }
}
