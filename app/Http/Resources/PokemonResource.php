<?php
namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PokemonResource extends JsonResource {
  /**
   * Transform the resource into an array.
   * @param  \Illuminate\Http\Request  $request
   * @return array
   */
  public function toArray($request) {
    return [
      'name' => $this->name,
      'pokemon_identifier' => $this->pokemon_identifier,
      'height' => $this->height,
      'weight' => $this->weight,
      'genus' => $this->genus,
      'captured' => $this->captured ?? false,
      'description' => $this->description,
      'types' => $this->types,
      'stats' => $this->stats,
      'egg_groups' => $this->eggGroups,
      'abilities' => $this->abilities,
    ];
  }
}
