<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ability extends Model {
  protected $guarded = [];
  /**
  * pokemon - Pokemon relationship
  * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
  */
  public function pokemon() {
    return $this->belongsToMany(Pokemon::class);
  }
}
