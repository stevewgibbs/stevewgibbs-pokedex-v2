<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Capture extends Model {
  protected $guarded = [];

  /**
   * user - user relationship
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function user() {
    return $this->belongsTo(User::class);
  }

  /**
   * pokemon - pokemon relationship
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function pokemon() {
    return $this->belongsTo(Pokemon::class, 'pokemon_id', 'pokemon_identifier');
  }
}
