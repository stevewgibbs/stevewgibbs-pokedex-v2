<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EggGroup extends Model {
  protected $guarded = [];
  /**
   * pokemon - pokemon relationship
   * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
   */
  public function pokemon() {
    return $this->belongsToMany(Pokemon::class);
  }
}
