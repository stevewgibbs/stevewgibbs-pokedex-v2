<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pokemon extends Model {
  protected $table = 'pokemon';
  protected $guarded = [];

  /**
   * types - types relationship
   * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
   */
  public function types() {
    return $this->belongsToMany(Type::class);
  }

  /**
   * abilities - relationship
   * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
   */
  public function abilities() {
    return $this->belongsToMany(Ability::class);
  }

  /**
   * eggGroups - relationship
   * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
   */
  public function eggGroups() {
    return $this->belongsToMany(EggGroup::class);
  }

  /**
   * stats - stats relationship
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function stats() {
    return $this->hasMany(Stat::class);
  }
}
