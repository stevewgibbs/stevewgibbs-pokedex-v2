<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stat extends Model {
  protected $guarded = [];

  /**
   * pokemon - relationship
   * @return \Illuminate\Database\Eloquent\Relations\HasOne
   */
  public function pokemon() {
    return $this->hasOne(Pokemon::class);
  }
}
