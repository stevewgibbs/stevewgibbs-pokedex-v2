<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Type extends Model {
  protected $guarded = [];

  /**
   * pokemon - relationship
   * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
   */
  public function pokemon() {
    return $this->belongsToMany(Pokemon::class);
  }
}
