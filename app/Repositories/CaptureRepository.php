<?php
namespace App\Repositories;

use App\Models\Capture;
use Carbon\Carbon;
use App\Repositories\PokemonRepository;

class CaptureRepository {
  /**
   * userCaptures - get captures by a user
   * @param $userId
   * @return mixed
   */
  public static function byUser($userId) {
    return Capture::where('user_id', $userId)
      ->orderBy('pokemon_id')
      ->with('pokemon', 'pokemon.types', 'pokemon.abilities', 'pokemon.eggGroups', 'pokemon.stats')
      ->get();
  }

  /**
   * isCaptured - check if pokemon has been captured by the user
   * @param $pokemonIdentifier
   * @return bool
   */
  public static function isCaptured($pokemonIdentifier) {
    return Capture::where('user_id', auth()->user()->id)
      ->where('pokemon_id', $pokemonIdentifier)
      ->with('pokemon')
      ->first() != null;
  }

  /**
   * create - create a capture record
   * @param Request $request
   * @return Capture
   */
  public static function create($request) {
    return Capture::firstOrCreate([
      'user_id' => auth()->user()->id,
      'pokemon_id' => $request->pokemon_identifier,
    ]);
  }
}
