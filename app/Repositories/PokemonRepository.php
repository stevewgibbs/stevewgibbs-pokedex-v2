<?php
namespace App\Repositories;

use App\Models\Pokemon;
use App\Models\Capture;
use App\Repositories\CaptureRepository;

class PokemonRepository {
  /**
   * byIdentifier - get pokemon by identifier
   * @param $pokemonIdentifier
   * @return mixed
   */
  public static function byIdentifier($pokemonIdentifier) {
    $pokemon = Pokemon::where('pokemon_identifier', $pokemonIdentifier)
      ->first();
    if($pokemon != null) {
      $pokemon->captured = CaptureRepository::isCaptured($pokemonIdentifier);
    }
    return $pokemon;
  }

  /**
   * byName - get pokemon by name
   * @param $name
   * @return mixed
   */
  public static function byName($name) {
    return Pokemon::where('name', 'LIKE', '%' . $name . '%')
      ->orderBy('name')
      ->take(config('app.per_page'))
      ->with('types', 'eggGroups', 'stats', 'abilities')
      ->get();
  }
}
