<?php
namespace App\Services;
use DB;
use App\Models\Pokemon;
use App\Models\Ability;
use App\Models\EggGroup;
use App\Models\Stat;
use App\Models\Type;

class Import {
  /**
   * executePokedex - import the pokedex data
   */
  public function executePokedex()
  {
    DB::transaction(function () {
      $file = fopen(storage_path("/") . "pokedex.csv", "r");
      while ($row = fgetcsv($file)) {
        $id = $row[0];
        $name = $row[1];
        $types = $row[2];
        $height = $row[3];
        $weight = $row[4];
        $abilities = $row[5];
        $eggGroups = $row[6];
        $stats = $row[7];
        $genus = $row[8];
        $description = $row[9];

        //skip heading row
        if ($stats != 'stats') {
          $pokemon = Pokemon::firstOrCreate([
            'pokemon_identifier' => $id,
            'name' => $name,
            'height' => $height,
            'weight' => $weight,
            'genus' => $genus,
            'description' => $description,
          ]);

          //Setup stats
          self::processStats($stats, $pokemon);
          //Setup abilities
          self::processAbilities($abilities, $pokemon);
          //Setup egg Groups
          self::processEggGroups($eggGroups, $pokemon);
          //Setup types
          self::processTypes($types, $pokemon);
        }
      }
      fclose($file);
    });
  }

  /**
   * processTypes - process pokemon types
   * @param $types
   * @param $pokemon
   */
  public static function processTypes($types, $pokemon)
  {
    $types = str_replace(["[", "]", "\""], "", $types);
    $typeObject = explode(",", $types);
    collect($typeObject)->each(function($type) use ($pokemon) {
      Type::firstOrCreate([
        'name' => trim($type),
      ])->pokemon()->syncWithoutDetaching($pokemon);
    });
  }

  /**
   * processEggGroups - process egg groups data
   * @param $eggGroups
   * @param $pokemon
   */
  public static function processEggGroups($eggGroups, $pokemon)
  {
    $eggGroups = str_replace(["[", "]", "\""], "", $eggGroups);
    $eggGroupObject = explode(",", $eggGroups);
    collect($eggGroupObject)->each(function($eggGroup) use ($pokemon) {
      EggGroup::firstOrCreate([
        'name' => trim($eggGroup),
      ])->pokemon()->syncWithoutDetaching($pokemon);
    });
  }

  /**
   * processStats - process stats data
   * @param $stats
   * @param $pokemon
   */
  public static function processStats($stats, $pokemon)
  {
    $statsObject = json_decode($stats);
    collect($statsObject)->each(function($value, $key) use ($pokemon) {
      Stat::firstOrCreate([
        'name' => $key,
        'value' => $value,
        'pokemon_id' => $pokemon->id,
      ]);
    });
  }

  /**
   * processAbilities - process abilities Data
   * @param $abilities
   * @param $pokemon
   */
  public static function processAbilities($abilities, $pokemon)
  {
    $abilities = str_replace(["[", "]", "\""], "", $abilities);
    $abilityObject = explode(",", $abilities);
    collect($abilityObject)->each(function($ability) use ($pokemon) {
      Ability::firstOrCreate([
        'name' => trim($ability),
      ])->pokemon()->syncWithoutDetaching($pokemon);
    });
  }
}
