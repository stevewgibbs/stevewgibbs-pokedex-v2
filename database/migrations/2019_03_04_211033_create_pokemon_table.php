<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePokemonTable extends Migration {
  /**
   * Run the migrations.
   * @return void
   */
  public function up() {
    Schema::create('pokemon', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('pokemon_identifier')
        ->nullable();
      $table->string('name')
        ->nullable();
      $table->float('height', 8, 2)
        ->nullable();
      $table->float('weight', 8, 2)
        ->nullable();
      $table->string('genus')
        ->nullable();
      $table->text('description')
        ->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('pokemon');
  }
}
