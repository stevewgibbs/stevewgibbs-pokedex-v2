<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePokemonTypeTable extends Migration {
  /**
   * Run the migrations.
   * @return void
   */
  public function up() {
    Schema::create('pokemon_type', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('pokemon_id')
        ->nullable();
      $table->integer('type_id')
        ->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   * @return void
   */
  public function down() {
    Schema::dropIfExists('pokemon_type');
  }
}
