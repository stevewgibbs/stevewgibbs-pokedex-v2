<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEggGroupPokemonTable extends Migration {
  /**
   * Run the migrations.
   * @return void
   */
  public function up() {
    Schema::create('egg_group_pokemon', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('egg_group_id')
        ->nullable();
      $table->integer('pokemon_id')
        ->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   * @return void
   */
  public function down() {
    Schema::dropIfExists('egg_group_pokemon');
  }
}
