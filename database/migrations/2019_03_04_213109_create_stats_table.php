<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatsTable extends Migration {
  /**
   * Run the migrations.
   * @return void
   */
  public function up() {
    Schema::create('stats', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('pokemon_id')
        ->default(0);
      $table->string('name')
        ->nullable();
      $table->string('value')
        ->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   * @return void
   */
  public function down() {
    Schema::dropIfExists('stats');
  }
}
