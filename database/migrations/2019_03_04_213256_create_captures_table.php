<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCapturesTable extends Migration {
  /**
   * Run the migrations.
   * @return void
   */
  public function up() {
    Schema::create('captures', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('user_id')
        ->nullable();
      $table->integer('pokemon_id')
        ->nullable();
      $table->timestamp('captured_at')
        ->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   * @return void
   */
  public function down() {
      Schema::dropIfExists('captures');
  }
}
