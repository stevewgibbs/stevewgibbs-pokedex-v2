# Group Pokemon Captures
## List Captures [GET /v1/pokedex/captures]
Returns a list of pokemon captures.

+ Response 200 (application/json)
  + Body
  
    {
      "data":[
        {
          "id": 200,
          "pokemon_identifier": 200,
          "name": "Misdreavus",
          "height": 7,
          "weight": 10,
          "genus": "Screech Pokémon",
          "description": "If you hear a sobbing sound emanating from a\nvacant room, it’s undoubtedly a bit of mischief\nfrom Misdreavus.",
          "created_at": "2019-03-09 20:40:58",
          "updated_at": "2019-03-09 20:40:58",
          "types": [
            {
              "id": 16,
              "name": "ghost",
              "created_at": "2019-03-09 20:40:53",
              "updated_at": "2019-03-09 20:40:53",
              "pivot": {
                "pokemon_id": 200,
                "type_id": 16
              }
            }
          ],
          "abilities": [
            {
              "id": 78,
              "name": "levitate",
              "created_at": "2019-03-09 20:40:53",
              "updated_at": "2019-03-09 20:40:53",
              "pivot": {
                "pokemon_id": 200,
                "ability_id": 78
              }
            }
          ],
          "egg_groups": [
            {
              "id": 13,
              "name": "indeterminate",
              "created_at": "2019-03-09 20:40:53",
              "updated_at": "2019-03-09 20:40:53",
              "pivot": {
                "pokemon_id": 200,
                "egg_group_id": 13
              }
            }
          ],
          "stats": [
            {
              "id": 1195,
              "pokemon_id": 200,
              "name": "hp",
              "value": "60",
              "created_at": "2019-03-09 20:40:58",
              "updated_at": "2019-03-09 20:40:58"
            },
            ...
          ]
        }
        ...
      ]
    }
   
## Capture Pokemon [POST /v1/pokedex/captures]
Create a new pokemon capture.

+ Request (application/json)
  + Body
  
    {
      "pokemon_identifier": 200
    }
    
+ Response
  + Body
  
    {
        "message": "Pokemon has been captured! Forretress - 2019-03-11 14:28:54"
    }