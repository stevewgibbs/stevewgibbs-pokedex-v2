# Group Pokedex 
## List Pokemon - Paged [GET /v1/pokedex]
Returns a list of pokemon.

+ Response 200 (application/json)
  + Body
  
    {
      "data":[
        {
          "id": 120,
          "pokemon_identifier": 120,
          "name": "Staryu",
          "height": 8,
          "weight": 345,
          "genus": "Star Shape Pokémon",
          "description": "This Pokémon gets nibbled on by Lumineon and\nothers. Thanks to its red core, it regenerates\nfast, so it’s unconcerned by their snack attacks.",
          "created_at": "2019-03-09 20:40:54",
          "updated_at": "2019-03-09 20:40:54"
        },
        ...
      ],
      "links": {
        "first": "http://localhost:8000/api/v1/pokedex?page=1",
        "last": "http://localhost:8000/api/v1/pokedex?page=24",
        "prev": "http://localhost:8000/api/v1/pokedex?page=4",
        "next": "http://localhost:8000/api/v1/pokedex?page=6"
      },
      "meta": {
        "current_page": 5,
        "from": 97,
        "last_page": 24,
        "path": "http://localhost:8000/api/v1/pokedex",
        "per_page": 24,
        "to": 120,
        "total": 553
      }
    }
      
## Get Pokemon by Identifier [GET /v1/pokedex/{pokemon_identifier}]
Returns the details of a pokemon.

+ Parameters

  + pokemon_identifier: `1` (int, required) - The Pokemon ID.
  
+ Response
  + Body
  
    {
      "data": {
        "name": "Charmeleon",
        "pokemon_identifier": 5,
        "height": 11,
        "weight": 190,
        "genus": "Flame Pokémon",
        "captured": false,
        "description": "Charmeleon mercilessly destroys its foes using its sharp\nclaws. If it encounters a strong foe, it turns aggressive.\nIn this excited state, the flame at the tip of its tail flares with\na bluish white color.",
        "types": [
          {
            "id": 3,
            "name": "fire",
            "created_at": "2019-03-09 20:40:50",
            "updated_at": "2019-03-09 20:40:50",
            "pivot": {
              "pokemon_id": 5,
              "type_id": 3
            }
          }
        ],
        "stats": [
          {
            "id": 25,
            "pokemon_id": 5,
            "name": "hp",
            "value": "58",
            "created_at": "2019-03-09 20:40:50",
            "updated_at": "2019-03-09 20:40:50"
          },
          ...
        ],
        "egg_groups": [
          {
            "id": 3,
            "name": "dragon",
            "created_at": "2019-03-09 20:40:50",
            "updated_at": "2019-03-09 20:40:50",
            "pivot": {
              "pokemon_id": 5,
              "egg_group_id": 3
            }
          },
          ...
        ],
        "abilities": [
          {
            "id": 3,
            "name": "solar-power",
            "created_at": "2019-03-09 20:40:50",
            "updated_at": "2019-03-09 20:40:50",
            "pivot": {
              "pokemon_id": 5,
              "ability_id": 3
            }
          },
          ...
        ]
      }
    }
  
## Get Pokemon by Name [GET /v1/pokedex/name/{name}]
Returns the list of pokemon that match the name search.
  
+ Parameters
  + name: Abs (string, required) - The Pokemon Name.
  
+ Response
  + Body
  
    {
      "data": [
        {
          "id": 460,
          "pokemon_identifier": 460,
          "name": "Abomasnow",
          "height": 22,
          "weight": 1355,
          "genus": "Frost Tree Pokémon",
          "description": "It lives a quiet life on mountains that are perpetually\ncovered in snow. It hides itself by whipping\nup blizzards.",
          "created_at": "2019-03-09 20:41:09",
          "updated_at": "2019-03-09 20:41:09",
          "types": [
            {
              "id": 15,
              "name": "ice",
              "created_at": "2019-03-09 20:40:53",
              "updated_at": "2019-03-09 20:40:53",
              "pivot": {
                "pokemon_id": 460,
                "type_id": 15
              }
            },
            ...
            }
          ],
          "egg_groups": [
            {
              "id": 1,
              "name": "plant",
              "created_at": "2019-03-09 20:40:50",
              "updated_at": "2019-03-09 20:40:50",
              "pivot": {
                "pokemon_id": 460,
                "egg_group_id": 1
              }
            },
            ...
          ],
          "stats": [
            {
              "id": 2755,
              "pokemon_id": 460,
              "name": "hp",
              "value": "90",
              "created_at": "2019-03-09 20:41:09",
              "updated_at": "2019-03-09 20:41:09"
            },
            ...
          ],
          "abilities": [
            {
              "id": 85,
              "name": "soundproof",
              "created_at": "2019-03-09 20:40:54",
              "updated_at": "2019-03-09 20:40:54",
              "pivot": {
                "pokemon_id": 460,
                "ability_id": 85
              }
            },
            ...
          ]
        }
      ]
    }