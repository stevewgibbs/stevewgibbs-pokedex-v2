# Group Trainers
## Get Trainer [GET /v1/trainers/me]
Returns the authenticated trainer

+ Response 200 (application/json)
  + Body
  
    {
      "data": {
        "id": 1,
        "first_name": "Steve",
        "last_name": "Doe",
        "email": "sdoe@gmail.com",
        "api_token": "TESTER",
        "is_admin": 1
      }
    }