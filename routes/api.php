<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['as' => 'v1.', 'prefix' => 'v1'], function () {
  Route::group(['namespace' => 'Api\V1', 'prefix' => 'pokedex', 'middleware' => 'auth:api'], function() {
    Route::group(['prefix' => 'captures'], function() {
      Route::get('/', 'CaptureController@index');
      Route::post('/', 'CaptureController@store');
    });

    Route::get('/', 'PokedexController@index');
    Route::get('/{pokemon_identifier}', 'PokedexController@show');
    Route::get('/name/{name}', 'PokedexController@byName');
  });

  Route::group(['namespace' => 'Auth', 'as' => 'auth.', 'prefix' => 'auth'], function () {
    Route::post('login', 'LoginController@login');
    Route::post('logout', 'LoginController@logout');
    Route::post('register', 'RegisterController@register');
    Route::post('reset-password', 'ResetPasswordController@reset');
    Route::post('forgot-password', 'ForgotPasswordController@sendResetLinkEmail');
  });

  Route::group(['middleware' => ['auth:api']], function () {
    Route::get('/trainers/me', function (Request $request) {
      return new \App\Http\Resources\TrainerResource($request->user());
    });
  });
});
