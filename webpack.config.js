"use strict";

const webpack = require('webpack');
const path = require('path');
const CleanWebpackPlugin = require("clean-webpack-plugin");
const ManifestPlugin = require("webpack-manifest-plugin");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const LiveReloadPlugin = require('webpack-livereload-plugin');
const merge = require('webpack-merge');
const lessToJs = require('less-vars-to-js');
const fs = require('fs');

const rpath = path.join.bind(path, __dirname);

const PATHS = {
  build: rpath("public", "build"),
  styles: rpath("resources", "assets"),
  vendorStyles: /node_modules/,
};

const modifyVars = lessToJs(fs.readFileSync(rpath('resources', 'assets', 'less', 'ant-theme.less'), 'ascii'));

// The public url prefix for webpack'd assets
const publicPath = '/build/';

const base = {

  entry: {
    app: './resources/assets/js/app.js',
  },

  output: {
    path: PATHS.build,
    filename: '[name].js'
  },

  module: {
    noParse: [/moment.js/], // Avoid including all moment locales (moment is pulled in by antd)

    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [{
          loader: 'babel-loader',
          options: {
            cacheDirectory: true
          },
        }]
      },

      {
        test: /\.less/,
        use: ExtractTextPlugin.extract({
          use: [
            {loader: "css-loader", options: { modules: false } },
            {loader: "less-loader", options: {modifyVars}}
          ],
          fallback: "style-loader"
        }),
        include: [PATHS.vendorStyles, PATHS.styles]
      },

      {test: /\.jpe?g$|\.gif$|\.png$|\.mp3$/, use: [{loader: "file-loader", options: {publicPath}}]},

      {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, use: [{loader: "file-loader", options: {publicPath}}]},

      {
        test: /\.woff(2)?(\?v=\d+\.\d+\.\d+)?$/, use: [{
        loader: "url-loader", options: {publicPath, limit: 10000, mimetype: "application/font-woff"}
      }]
      },

      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, use: [{
        loader: "url-loader", options: {publicPath, limit: 10000, mimetype: "application/octet-stream"}
      }]
      },

      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        use: [{loader: "url-loader", options: {publicPath, limit: 10000, mimetype: "image/svg+xml"}}]
      }
    ]

  },

  resolve: {
    alias: {
      components: rpath('resources', 'assets', 'js', 'components'),
      screens: rpath('resources', 'assets', 'js', 'screens'),
      utils: rpath('resources', 'assets', 'js', 'utils'),
      less: rpath('resources', 'assets', 'less'),
      images: rpath('resources', 'assets', 'images'),
    },
    extensions: [ '.js', '.less']
  },

  plugins: [
    //new BundleAnalyzerPlugin(),
    new ManifestPlugin({
      publicPath: rpath("public"),
      fileName: "mix-manifest.json",
      basePath: '/'
    }),
  ]

};


let config;

switch (process.env.NODE_ENV) {
  case "production":
    config = merge(
      base,
      {
        output: {
          path: PATHS.build,
          filename: "[name]-[chunkhash].js",
          publicPath: PATHS.build
        },
        devtool: "source-map",
        plugins: [
          new webpack.DefinePlugin({
            "process.env": {
              "NODE_ENV": JSON.stringify("production")
            }
          }),
          new webpack.optimize.UglifyJsPlugin({
            comments: false,
            sourceMap: true,
            compress: {
              warnings: false
            }
          }),
          new ExtractTextPlugin({
            filename: "[name]-[contenthash].css",
            allChunks: true
          })
        ]
      }
    );
    break;

  default:
    config = merge(base, {
      devtool: "eval-source-map",
      plugins: [
        new ExtractTextPlugin({
          filename: '[name].css',
          allChunks: true
        }),
        new CleanWebpackPlugin([PATHS.build], {root: process.cwd()}),
        new LiveReloadPlugin({
          appendScriptTag: true
        })
      ]
    });
}

module.exports = config;
